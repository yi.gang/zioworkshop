package com.gitlab.kzs.zioworkshop.stream

import java.nio.file.Paths

import cats.effect.Resource
import com.gitlab.kzs.zioworkshop.STask
import com.gitlab.kzs.zioworkshop.model.Stock
import fs2.{Stream, io, text}
import zio.Task
import zio.interop.catz._

import scala.concurrent.ExecutionContextExecutorService

trait FileStream {
  def salesFromFile: Stream[STask, Stock]
}

// We could also stream content from an external webservice here, using a file to keep the exercice simple
class FileStreamLive(path: String, blockingExecutionContext: Resource[Task, ExecutionContextExecutorService]) extends FileStream {

  def salesFromFile: Stream[STask, Stock] = {
    Stream.resource(blockingExecutionContext).flatMap { blockingEC =>
      io.file.readAll[STask](Paths.get(path), blockingEC, 4096)
        .through(text.utf8Decode)
        .through(text.lines)
        .filter(s => !s.trim.isEmpty && !s.startsWith("//"))
        .map(lineToStock)
    }
  }
  private def lineToStock(line: String): Stock = line.split(",") match {
    case Array(id, value) =>  Stock(id.toInt, value.toInt)
  }

}

